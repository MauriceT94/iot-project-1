#!/bin/bash

DIRECTORYTOLIST=/tmp #This is the default if no parameter $1 is given.

if [ "$#" -eq 1 -a -e "$1" ]
then
    if [ -d "$1" ] ; then
    DIRECTORYTOLIST=$1
    else
        echo "Directory $1 not found. :("
        exit 1
    fi
fi

for $*
    do 
    echo $*
    else
    cat $* >> HTMLOUTPUT
fi 

ls $DIRECTORYTOLIST