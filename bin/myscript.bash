#!/bin/bash



HTMLOUTPUT=/opt/iotproject/index.html
DIRECTORYTOLIST=/tmp #This is the default if no parameter $1 is given.

if [ "$#" -ge 1 ]
then
    if [ -d "$1" ] 
    then
    DIRECTORYTOLIST=$1
    else
        echo "Directory $1 not found. :("
        exit 1
    fi
fi
shift   

echo "<html><body>" > $HTMLOUTPUT
echo "<h1>Mein Webserver</h1>" >> $HTMLOUTPUT
date +%H:%M:%S >> $HTMLOUTPUT 
ls $DIRECTORYTOLIST >> $HTMLOUTPUT
#For-Loop der die Existenz der Files erst prüft, dann anhaengt oder Fehlermeldung ausgiebt. 
for htmlsnippet in $*
do 
    if [ -r "$htmlsnippet" ]
    then
    cat $htmlsnippet >> $HTMLOUTPUT
    else
        echo "File $htmlsnippet is not readable" >&2
    fi 
done


echo "</body></html>" >> $HTMLOUTPUT